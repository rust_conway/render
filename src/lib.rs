#[macro_use]
extern crate gfx;
extern crate gfx_core;
extern crate gfx_window_glutin;
extern crate gfx_device_gl;
extern crate glutin;

gfx_defines!{
    vertex Vertex {
        pos: [f32; 4] = "a_Pos",
        color: [f32; 3] = "a_Color",
    }

    constant Transform {
        transform: [[f32; 4]; 4] = "u_Transform",
    }

    pipeline pipe {
        vbuf: gfx::VertexBuffer<Vertex> = (),
        transform: gfx::ConstantBuffer<Transform> = "Transform",
        out: gfx::RenderTarget<ColorFormat> = "Target0",
    }
}

pub use gfx::traits::FactoryExt;
pub use gfx::Device;
// use gfx_window_glutin as gfx_glutin;

use std::time::Instant;
use std::thread;

pub type ColorFormat = gfx::format::Srgba8;
pub type DepthFormat = gfx::format::DepthStencil;

pub const BLACK: [f32; 4] = [0.0, 0.0, 0.0, 1.0];
pub const WHITE: [f32; 4] = [1.0; 4];

struct Quad {
    x: f32,
    y: f32,
    width: f32,
    height: f32,
    color: [f32; 3]
}

pub struct ConwaySpawn {
    quads: Vec<Quad>,
    pub ratio: f32,
    pub cw_field: conway_core::Field,
}

pub const TRANSFORM: Transform = Transform {
    transform: [[1.0, 0.0, 0.0, 0.0],
                [0.0, 1.0, 0.0, 0.0],
                [0.0, 0.0, 1.0, 0.0],
                [0.0, 0.0, 0.0, 1.0]]
};

impl ConwaySpawn {
    pub fn new(cw_field: conway_core::Field) -> Self {
        let mut quads = vec![];
        let ratio = 1.0;
        for y in (0..cw_field.size.height).rev() {
            for x in 0..cw_field.size.width {
                let index = (cw_field.size.height - y - 1) * cw_field.size.width + x;
                let color = match cw_field.field[index] {
                    conway_core::Status::Populated => [1.0, 1.0, 1.0],
                    conway_core::Status::Unpopulated => [0.0, 0.0, 0.0]
                };
                let x = -1.0 + x as f32 * (2.0 / cw_field.size.width as f32);
                let y = -1.0 + y as f32 * (2.0 / cw_field.size.height as f32);
                let width = 0.9 / cw_field.size.width as f32;
                let height = 0.9 / cw_field.size.height as f32;

                quads.push(Quad{ x, y, width, height, color});
            }
        }
        Self { quads, cw_field, ratio }
    }

    pub fn add_quad(&mut self, x: f32, y: f32, width: f32, color: [f32; 3]) {
        let elem = Quad {x, y, width, height: width, color};
        self.quads.push(elem);
    }

    pub fn set_ratio(&mut self, ratio_new: f32) {
        self.ratio = ratio_new;
    }
    
    pub fn next_step(&mut self) {
        self.cw_field.next_step();
        for (i, elem) in self.cw_field.field.iter().enumerate() {
            match elem {
                conway_core::Status::Unpopulated => self.quads[i].color = [0.0, 0.0, 0.0],
                conway_core::Status::Populated => self.quads[i].color = [1.0, 1.0, 1.0]
            };
        }
    }

    pub fn get_slices_indices(&self) -> (Vec<Vertex>, Vec<u16>) {
        let (mut vertex, mut is) = (vec![], vec![]);
        let field_ratio = self.cw_field.size.width as f32 / self.cw_field.size.height as f32;
        let ratio = self.ratio / field_ratio;

        for (i, elem) in self.quads.iter().enumerate() {
            let i = i as u16;

            let (wx, wy, pos_x, pos_y);
            if ratio > 1.0 {
                wx = elem.width / ratio; 
                wy = elem.height;
                pos_x = elem.x / ratio;
                pos_y = elem.y;

            } else {
                wx = elem.width;
                wy = elem.height * ratio;
                pos_x = elem.x;
                pos_y = elem.y * ratio;
            }

            vertex.extend(&[
                          Vertex { pos: [pos_x - wx, pos_y - wy, 0.0, 1.0], color: elem.color },
                          Vertex { pos: [pos_x - wx, pos_y + wy, 0.0, 1.0], color: elem.color },
                          Vertex { pos: [pos_x + wx, pos_y - wy, 0.0, 1.0], color: elem.color },
                          Vertex { pos: [pos_x + wx, pos_y + wy, 0.0, 1.0], color: elem.color }
            ]);
            is.extend(&[ i * 4 + 0, i * 4 + 1, i * 4 + 2, i * 4 + 1, i * 4 + 2, i * 4 + 3]);
        }
        (vertex, is)
    }
}

pub struct MainWindow {
    events_loop: glutin::EventsLoop,
    window: glutin::WindowedContext<glutin::PossiblyCurrent>,
    device: gfx_device_gl::Device,
    factory: gfx_device_gl::Factory,
    color_view: gfx_core::handle::RenderTargetView<gfx_device_gl::Resources, ColorFormat>,
    depth_view: gfx_core::handle::DepthStencilView<gfx_device_gl::Resources, DepthFormat>,
}


unsafe impl Send for MainWindow {}

impl MainWindow {
    fn new() -> MainWindow {
        let events_loop = glutin::EventsLoop::new();
        let windowbuilder = glutin::WindowBuilder::new()
            .with_title("Conway test".to_string())
            .with_dimensions(glutin::dpi::LogicalSize::new(1366.0, 768.0));
        let contextbuilder = glutin::ContextBuilder::new();
        let (window, device, factory, color_view, depth_view) = 
            gfx_window_glutin::init::<ColorFormat, DepthFormat>(windowbuilder, contextbuilder, &events_loop)
            .expect("Failed to create window");
        MainWindow{events_loop, window, device, factory, color_view, depth_view}
    }

    fn get_pipe(&mut self) -> gfx::pso::PipelineState<gfx_device_gl::Resources, pipe::Meta> {
        self.factory.create_pipeline_simple(
            include_bytes!("shader/myshader_150.glslv"),
            include_bytes!("shader/myshader_150.glslf"),
            pipe::new()).unwrap()
    }

    pub fn main_loop(mut qs: ConwaySpawn) -> std::thread::JoinHandle<i8> {
        thread::spawn(move || {
            let mut mw = MainWindow::new();
            let mut encoder: gfx::Encoder<_, _> = mw.factory.create_command_buffer().into();
            let transform_buffer = mw.factory.create_constant_buffer(1);

            let (vert, ind) = qs.get_slices_indices();
            let (vertex_buffer, mut slice) = mw.factory.create_vertex_buffer_with_slice(&vert, &*ind);


            let mut data = pipe::Data {
                vbuf: vertex_buffer,
                transform: transform_buffer,
                out: mw.color_view.clone(),
            };

            let mut running = true;
            let mut needs_update = false;
            let now = Instant::now();
            let mut counter = 0;

            let mut fps = true;
            let mut next_step = true;

            while running {
                if needs_update {
                    gfx_window_glutin::update_views(&mw.window, &mut data.out, &mut mw.depth_view);
                    let (vs, is) = qs.get_slices_indices();
                    let (vbuf, sl) = mw.factory.create_vertex_buffer_with_slice(&vs, &*is);
                    data.vbuf = vbuf;
                    slice = sl;
                    needs_update = false;
                }

                // ======================================
                // move these timers to their own thread
                if now.elapsed().as_millis() % 100 < 20 && next_step == false {
                    qs.next_step();
                    needs_update = true;
                    next_step = true;
                } else {
                    next_step = false;
                }
                if now.elapsed().as_millis() % 1000 < 20 && fps == false {
                    println!("ups: {}", counter);
                    counter = 0;
                    fps = true;
                } else {
                    fps = false;
                }
                // ======================================

                mw.events_loop.poll_events( |event| {
                    if let glutin::Event::WindowEvent { event, .. } = event {
                        match event{
                            glutin::WindowEvent::CloseRequested | glutin::WindowEvent::KeyboardInput {
                                input: glutin::KeyboardInput {
                                    virtual_keycode: Some(glutin::VirtualKeyCode::Escape), ..  }, ..  }
                                => running = false,
                            glutin::WindowEvent::Resized(new_size) => {
                                qs.set_ratio(new_size.width as f32 / new_size.height as f32);
                                needs_update = true;
                            },
                            glutin::WindowEvent::MouseInput {
                                state: glutin::ElementState::Pressed, button: glutin::MouseButton::Left, modifiers: _, device_id: _} => {
                                    qs.next_step();
                                    needs_update = true;
                                }
                            _ => {}
                        }
                    }
                });
                encoder.clear(&mw.color_view, BLACK);
                encoder.update_buffer(&data.transform, &[TRANSFORM], 0).unwrap();

                // cant draw here?
                encoder.draw(&slice, &mw.get_pipe(), &data);
                encoder.flush(&mut mw.device);
                mw.window.swap_buffers().unwrap();
                mw.device.cleanup();

                // increment ups counter
                counter += 1;
            }
            return 0;
        })
    }
}
