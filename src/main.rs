extern crate conway_core;
extern crate conway_templates;

// use std::time::Instant;
use conway_templates as templates;
use rust_conway_render::*;

fn main() {
    let qs = ConwaySpawn::new(templates::new_gosper_slider_gun());
    let handle = MainWindow::main_loop(qs);
    // can add more stuff here
    // send signals to thread maybe
    handle.join().expect("Unable to join, error in mainwindow");
}
